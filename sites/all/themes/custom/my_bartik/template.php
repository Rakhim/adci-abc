<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Views Slideshow: pager.
 *
 * @ingroup themeable
 */
function my_bartik_theme_views_slideshow_pager_widget_render($vars) {
  // Add javascript settings for the pager type.
  $js_vars = array(
    'viewsSlideshowPager' => array(
      $vars['vss_id'] => array(
        $vars['location'] => array(
          'type' => views_slideshow_format_addons_name($vars['settings']['type'])
        ),
      ),
    ),
  );

  drupal_add_library('views_slideshow', 'views_slideshow');
  drupal_add_js($js_vars, 'setting');
 
  // Create some attributes
  $attributes['class'] = 'QWERTYU widget_pager widget_pager_' . $vars['location'];
  $attributes['id'] = 'widget_pager_' . $vars['location'] . '_' . $vars['vss_id'];
  return theme(views_theme_functions($vars['settings']['type'], $vars['view'], $vars['view']->display[$vars['view']->current_display]), array('vss_id' => $vars['vss_id'], 'view' => $vars['view'], 'settings' => $vars['settings'], 'location' => $vars['location'], 'attributes' => $attributes));
}
