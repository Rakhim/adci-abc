/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {
  
  Drupal.behaviors.nauthors_disabled = {
    attach: function (context, settings) {
      
      $('#edit-nauthors-check-all', context).click(function(e){
        if($(this).is(":checked")){
            $(".nauthors_nodes .form-checkbox").attr("disabled", true);
        }
        else {
            $(".nauthors_nodes .form-checkbox").attr("disabled", false); 
            // input[name*='nauthors_nodes']
            // .form-checkbox
        }       
      });
      
    }
  };
  
})(jQuery);